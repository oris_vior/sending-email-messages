FROM openjdk:17-alpine

RUN apk update && apk upgrade

WORKDIR /app
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} ./app.jar
ENV SPRING_KAFKA_BOOTSTRAP-SERVERS=kafka:9092
ENV SPRING_DATA_ELASTICSEARCH_CLUSTER-NAME=my_elasticsearch
ENV SPRING_DATA_ELASTICSEARCH_CLUSTER-NODES=elasticsearch:9200

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app/app.jar"]
