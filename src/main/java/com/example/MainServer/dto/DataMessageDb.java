package com.example.MainServer.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;


@Getter
@Setter
@Data
public class DataMessageDb {
    private String subject;
    private String message;
    private String receiver;
    private Boolean successful;
    private String textError;
}
