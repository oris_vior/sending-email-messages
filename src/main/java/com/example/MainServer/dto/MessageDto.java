package com.example.MainServer.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

@Getter
@Setter
@Jacksonized
@RequiredArgsConstructor
public class MessageDto  {
    private String subject;
    private String message;
    private List<String> receiver;

    private String successful;
}
