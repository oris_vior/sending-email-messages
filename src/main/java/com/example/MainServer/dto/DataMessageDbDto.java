package com.example.MainServer.dto;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;


@Getter
@Setter
@Data
@Document(indexName="message")
public class DataMessageDbDto {
    @Id
    private String id;
    @Field(type = FieldType.Text)
    private String subject;
    @Field(type = FieldType.Text)
    private String message;
    @Field(type = FieldType.Text)
    private String receiver;
    @Field(type = FieldType.Keyword)
    private Boolean successful;
    @Field(type = FieldType.Text)
    private String textError;
}
