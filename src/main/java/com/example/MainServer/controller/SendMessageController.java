package com.example.MainServer.controller;

import com.example.MainServer.dto.DataMessageDb;
import com.example.MainServer.dto.DataMessageDbDto;
import com.example.MainServer.dto.MessageDto;
import com.example.MainServer.service.AddDbServ;
import com.example.MainServer.service.AddDbService;
import com.example.MainServer.service.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class SendMessageController {
    @Autowired
    MessageService messageService;

    @Autowired
    AddDbServ addDbServ;


    @PostMapping("/send")
    public void send(@RequestBody MessageDto message) {
        messageService.sendMessage(message);
        System.out.println("OK");
    }

}
