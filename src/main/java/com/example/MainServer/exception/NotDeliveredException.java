package com.example.MainServer.exception;

public class NotDeliveredException extends RuntimeException{
    @Override
    public String getMessage() {
        return "message not delivered";
    }
}
