package com.example.MainServer.service;

import com.example.MainServer.dto.DataMessageDbDto;
import com.example.MainServer.dto.MessageDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MessageServiceIImpl implements MessageService{
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;



    @SneakyThrows
    public void sendMessage(MessageDto msg) {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(msg);
        kafkaTemplate.send("topic", json);
    }
}
