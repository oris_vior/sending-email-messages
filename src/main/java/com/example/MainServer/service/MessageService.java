package com.example.MainServer.service;

import com.example.MainServer.dto.MessageDto;

public interface MessageService {
    void sendMessage(MessageDto messageDto);

}
