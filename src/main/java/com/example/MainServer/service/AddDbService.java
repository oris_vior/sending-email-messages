package com.example.MainServer.service;

import com.example.MainServer.dto.DataMessageDb;
import com.example.MainServer.dto.DataMessageDbDto;
import com.example.MainServer.repository.MyRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AddDbService implements AddDbServ {
    @Autowired
    MyRep myRep;


    @Override
    public void add(DataMessageDb dataMessageDb) {
        myRep.save(convertMessage(dataMessageDb));
    }

    private DataMessageDbDto convertMessage(DataMessageDb dataMessageDb) {
        DataMessageDbDto dataMessageDbDto = new DataMessageDbDto();
        dataMessageDbDto.setMessage(dataMessageDb.getMessage());
        dataMessageDbDto.setSubject(dataMessageDb.getSubject());
        dataMessageDbDto.setReceiver(dataMessageDb.getReceiver());
        dataMessageDbDto.setTextError(dataMessageDb.getTextError());
        return dataMessageDbDto;
    }

    public List<DataMessageDbDto> findFiledMessage(Boolean check) {
        return myRep.findBySuccessful(check);
    }
}
