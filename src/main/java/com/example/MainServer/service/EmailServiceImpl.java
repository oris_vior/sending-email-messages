package com.example.MainServer.service;

import com.example.MainServer.dto.DataMessageDb;
import com.example.MainServer.dto.DataMessageDbDto;
import com.example.MainServer.dto.MessageDto;
import com.example.MainServer.exception.NotDeliveredException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private AddDbServ addDbServ;


    @SneakyThrows
    @Override
    public void sendMessage(String message) {
        ObjectMapper objectMapper = new ObjectMapper();
        MessageDto messageDto = objectMapper.readValue(message, MessageDto.class);
        messageDto.getReceiver()
                .forEach(receiver -> useSmtpServer(receiver, messageDto.getSubject(), messageDto.getMessage()));
    }


    public void useSmtpServer(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        try {
            javaMailSender.send(message);
            DataMessageDb dataMessageDb = createDataMessage(to, subject, text, "", true);
            addDbServ.add(dataMessageDb);
        } catch (NotDeliveredException e) {
            DataMessageDb dataMessageDb = createDataMessage(to, subject, text, e.getMessage(), false);
            addDbServ.add(dataMessageDb);
        }

    }

    private DataMessageDb createDataMessage(String to, String subject, String text, String textError, Boolean successful) {
        DataMessageDb dataMessageDb = new DataMessageDb();
        dataMessageDb.setMessage(text);
        dataMessageDb.setSubject(subject);
        dataMessageDb.setReceiver(to);
        dataMessageDb.setSuccessful(successful);
        dataMessageDb.setTextError(textError);
        return dataMessageDb;
    }

    @Transactional
    @Scheduled(fixedDelay = 300000) // запускається кожні 5 хвилин
    public void resendFailedMessages() {
        List<DataMessageDbDto> failedMessages = addDbServ.findFiledMessage(false);
        for (DataMessageDbDto message : failedMessages) {
            try {
                useSmtpServer(message.getReceiver(), message.getSubject(), message.getMessage());
            } catch (Exception e) {
                DataMessageDb dataMessageDb = createDataMessage(message.getReceiver(), message.getSubject(), message.getMessage(), e.getMessage(), false);
                addDbServ.add(dataMessageDb);
            }
        }
    }
}
