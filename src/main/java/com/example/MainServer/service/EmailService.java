package com.example.MainServer.service;

import com.example.MainServer.dto.DataMessageDbDto;

import java.util.Optional;

public interface EmailService {
    void sendMessage(String message);
}
