package com.example.MainServer.service;

import com.example.MainServer.dto.DataMessageDb;
import com.example.MainServer.dto.DataMessageDbDto;

import java.util.List;

public interface AddDbServ {
    void add(DataMessageDb dataMessageDb);

    List<DataMessageDbDto> findFiledMessage(Boolean check);
}
