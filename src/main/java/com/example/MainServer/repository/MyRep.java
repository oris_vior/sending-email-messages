package com.example.MainServer.repository;

import com.example.MainServer.dto.DataMessageDbDto;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface MyRep extends ElasticsearchRepository<DataMessageDbDto,String> {
    List<DataMessageDbDto> findBySuccessful(Boolean s);
}
