package com.example.MainServer.listner;

import com.example.MainServer.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class Listener {

    @Autowired
    EmailService emailService;
    @KafkaListener(topics = "${spring.kafka.consumer.topic}", groupId = "foo")
    public void getMessage(String message) {
        System.out.println(message);
        emailService.sendMessage(message);
    }
}
